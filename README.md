# Into to the Crossref APIverse

This quarto document presents a set of examples illustrating how to use the Crossref API for retrieving metadata within the R coding environment.

Visit the notebook [here](https://crossref.gitlab.io/tutorials/intro_to_cr_api)

